# MicroProfile generated Application

## Introduction

MicroProfile Starter has generated this MicroProfile application for you.

The generation of the executable jar file can be performed by issuing the following command

    mvn clean package

This will create an executable jar file **cours2-exec.jar** within the _target_ maven folder. This can be started by executing the following command

Another option is to clean, build and launch with a single command

	mvn clean package tomee:run

Then you can modify the code and use the build from the ide (without restarting) or

    mvn build



## Specification examples


	
Note : pour creer mes requtes j'ai utilisé l'outil insomnia , dans le dossier ressource il y a un fichier insomnia.json que vous pouvez importer pour avoir
acces  toutez les requtes facilement si vous utilisé aussi insomnia

Note2 : le fichier de spécification et le diagramme de classe sont à la racine de ce projet

### Some curl requests
	creer un utilisateur 
	curl -i -X POST -H "Content-Type: application/json" -d "{\"email\":\"toto@toto.org\",\"age\":18,\"firstname\":\"bob\",\"lastname\":\"dylan\",\"password\":\"toto\"}" http://localhost:8080/data/users 
	se connecter :
	 curl "http://localhost:8080/data/users/login?email=toto@toto.org&password=toto" > jwt.txt 
	creer un groupe :
	curl -i -X POST -H@jwt.txt -H "Content-Type: application/json" -d "{\"name\":\"monGroupe\"}" http://localhost:8080/data/users/1/groups
	creer un album :
	curl -i -X POST -H@jwt.txt -H "Content-Type: application/json" -d "{\"name\":\"monAlbum\",\"description\":\"blabla\"}" http://localhost:8080/data/users/1/albums
	creer une photo 
	curl -H@jwt.txt --header "Content-Type:image/jpeg"  --data-binary @C:\Users\pcguillaume\Pictures\iae.jpg http://localhost:8080/data/albums/3/photos
	Obtenir toutes les photos de l'album 
	curl -X GET -H@jwt.txt -d "" http://localhost:8080/data/albums/2/photos
	supprimer une photo 
	curl -X DELETE -H@jwt.txt -d "" http://localhost:8080/data/albums/11/photos/15
	supprimer un album 
	curl -X DELETE -H@jwt.txt -d "" http://localhost:8080/data/albums/17
	Ajouter un ou plusieus membres  un groupe 
	curl -X POST -H@jwt.txt -H "Content-Type: application/json" -d "["6"]" http://localhost:8080/data/groups/8
	Supprimer un ou plusieurs membre d'un groupe 
	curl -X PATCH -H@jwt.txt -H "Content-Type: application/json" -d "["6"]" http://localhost:8080/data/groups/8
	Supprimer un groupe 
	curl -X DELETE -H@jwt.txt -d "" http://localhost:8080/data/users/1/groups/2
	Partager un album 
	curl -i -X POST -H@jwt.txt -H "Content-Type: application/json" -d "[\"2\"]" http://localhost:8080/data/groups/3/albums
	Obtenir les albums partags avec le groupe 2
	curl -X GET -H@jwt.txt -d "" http://localhost:8080/data/groups/2/albums
	obtenir la liste de tout les memebres d'un groupe
	curl -X GET -H@jwt.txt -d "" http://localhost:8080/data/groups/8
	lister tous les groupes
	curl -X GET -H@jwt.txt -d "" http://localhost:8080/data/groups
	lister tout les albums d'un utilisateur 
	curl -X GET -H@jwt.txt -d "" http://localhost:8080/data/users/1/albums
	obtenir un album d'un d'utilisateur
	curl -X GET -H@jwt.txt -d "" http://localhost:8080/data/users/1/albums/2
	obtenir tous les groupes  d'un utilisateur 
	curl -X GET -H@jwt.txt -d "" http://localhost:8080/data/users/1/groups
	lister tous les utilisateurs
	curl -X GET -H@jwt.txt -d "" http://localhost:8080/data/users
	Supprimer un utilisateur 
	curl -X DELETE -H@jwt.txt -d "" http://localhost:8080/data/users/1
	Obtenir tout les groupes dont un utilisateur est membre
	curl -X GET -H@jwt.txt -d "" http://localhost:8080/data/users/9/member
	ne plus partager un album avec un ou plusieurs groupes 
	curl -X PATCH -H@jwt.txt "Content-Type: application/json" -d "[\"7\",\"19\"]" http://localhost:8080/data/albums/18/shared
	Partager un album avec un ou plusieurs groupes
	curl -X POST -H@jwt.txt "Content-Type: application/json" -H "Authorization: Bearer " -d "[\"8\"]" http://localhost:8080/data/albums/20/shared
	Supprimer un ou plusieurs membres d'un groupe
	curl -X PATCH -H@jwt.txt "Content-Type: application/json" -d "[\"8\"]" http://localhost:8080/data/groups/8
	Telecharger une photo 
	curl -o testDownload.png -H@jwt.txt  http://localhost:8080/data/albums/2/photos/3
	
	
	
	
	



	













