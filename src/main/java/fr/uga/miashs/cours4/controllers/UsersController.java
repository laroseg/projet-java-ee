package fr.uga.miashs.cours4.controllers;

import fr.uga.miashs.cours4.dao.AlbumsDao;
import fr.uga.miashs.cours4.dao.GroupsDao;
import fr.uga.miashs.cours4.dao.UsersDao;
import fr.uga.miashs.cours4.model.AppAlbum;
import fr.uga.miashs.cours4.model.AppGroup;
import fr.uga.miashs.cours4.model.AppUser;
import fr.uga.miashs.cours4.security.IsOwner;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

import java.util.List;
import java.util.Map;

@Path("/users")
public class UsersController {

	@Inject
	private GroupsDao groupsDao;

	@Inject
	private UsersDao usersDao;
	@Inject
	private AlbumsDao albumsDao;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response create(AppUser obj, @Context UriInfo uriInfo) {
		return usersDao.create(obj, uriInfo);
	}

	@GET
	@Path("/login")
	public Response login(@QueryParam("email") String email, @QueryParam("password") String password) {
		return usersDao.login(email, password);
	}

	@RolesAllowed("user")
	@GET
	@Path("/{id: [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	@IsOwner
	public AppUser read(@PathParam("id") long id) {
		return usersDao.read(id);
	}
	
	@RolesAllowed("user")
	@GET
	@Path("/{id: [0-9]+}/member")
	@Produces(MediaType.APPLICATION_JSON)
	@IsOwner
	public Response isMemberOf(@PathParam("id") long id) {
		return Response.ok().entity(groupsDao.IsMemberOf(id)).build();
	}
	
	
	@RolesAllowed("user")
	@PATCH
	@PUT
	@Path("/{id: [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@IsOwner
	public Response update(@PathParam("id") long id, Map<String, Object> obj, @Context UriInfo uriInfo) {
		return usersDao.update(id, obj, uriInfo);
	}
	
	@RolesAllowed("admin")
	@DELETE
	@Path("/{id: [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") long id) {
		return usersDao.delete(id);
	}
	@RolesAllowed("admin")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
		return usersDao.listAll(uriInfo, limit, offset);
	}
	@RolesAllowed("user")
	@POST
	@Path("/{id: [0-9]+}/groups")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@IsOwner
	public Response createGroup(@PathParam("id") long userId, AppGroup g, @Context UriInfo uriInfo) {
		return groupsDao.createGroup(userId, g, uriInfo);
	}
	
	@RolesAllowed("user")
	@GET
	@Path("/{id: [0-9]+}/groups")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@IsOwner
	public Response getGroups(@PathParam("id") long userId, @Context UriInfo uriInfo, @PathParam("limit") int limit,
			@PathParam("offset") int offset) {
		return groupsDao.getGroups(userId, uriInfo, limit, offset);
	}
	@RolesAllowed("user")
	@DELETE
	@Path("/{id: [0-9]+}/groups/{groupId: [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@IsOwner
	public Response deleteGroup(@PathParam("id") long id, @PathParam("groupId") long idGroup) {
		return groupsDao.delete(idGroup);
	}
	@RolesAllowed("user")
	@POST
	@Path("/{id: [0-9]+}/albums")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@IsOwner
	public Response createAlbum(@PathParam("id") long userId, AppAlbum album, @Context UriInfo uriInfo) {
		return albumsDao.createAlbum(userId, album, uriInfo);
	}
	@RolesAllowed("user")
	@GET
	@Path("/{id: [0-9]+}/albums")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@IsOwner
	public Response getAlbums(@PathParam("id") long userId, @Context UriInfo uriInfo) {
		

		Response r = albumsDao.getAlbums(userId, uriInfo, 10, 0);
		List<AppAlbum> l = (List<AppAlbum>) r.getEntity();
		for (AppAlbum a : l) {
			a.setOwner(null);
		}
		return Response.ok().entity(l).build();
	}
	
	
	@GET
	@Path("/{id: [0-9]+}/albums/{idAlbum : [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	@RolesAllowed("user")
	@IsOwner
	public AppAlbum getAlbum(@PathParam("id") long userId, @PathParam("idAlbum") long albumId) {

		return albumsDao.getAlbum(albumId);
	}

}
