package fr.uga.miashs.cours4.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.ClaimValue;
import org.eclipse.microprofile.jwt.Claims;

import fr.uga.miashs.cours4.dao.AlbumsDao;
import fr.uga.miashs.cours4.dao.GroupsDao;
import fr.uga.miashs.cours4.dao.PhotosDao;
import fr.uga.miashs.cours4.errors.NotOwnerException;
import fr.uga.miashs.cours4.model.AppAlbum;

@Path("/albums")
@RolesAllowed("user")
public class AlbumsController {

	@Inject
	private AlbumsDao albumsDao;
	@Inject
	private PhotosDao photosDao;
	@Inject
	private GroupsDao groupsDao;
	
	@Inject
	@Claim(standard = Claims.sub)
	protected ClaimValue<String> connectedUserEmail;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id : [0-9]+}/photos")
	public Response getAll(@PathParam("id") long albumId,@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
		return photosDao.listAll(albumId,uriInfo, limit, offset);
		
	}

	@Path("/{id : [0-9]+}/photos")
	@POST
	@Consumes({ "image/jpeg", "image/png" })
	public Response uploadPhotos(InputStream inputStream, @PathParam("id") long albumId, @Context UriInfo uriInfo,
			@HeaderParam("Content-Type") String fileType) {

		return photosDao.addPhoto(albumId, uriInfo, inputStream, fileType);

	}

	@GET
	@Path("/{id : [0-9]+}/photos/{idPhoto:[0-9]+}")
	@Produces({ "image/jpeg", "image/png",MediaType.APPLICATION_JSON })
	public InputStream getPhoto(@PathParam("id") long albumId, @PathParam("idPhoto") long photoId) throws IOException {
		AppAlbum a = albumsDao.read(albumId);
		try {
			if(!a.getOwner().getEmail().equals(connectedUserEmail.getValue())){
				groupsDao.checkIfMember(connectedUserEmail.getValue(),a);
			}
		}
		catch(NotOwnerException e) {
			throw new ForbiddenException();
			// le try catch est la car sinon cot� client j'ai cette erreur :
			//No message body writer has been found for class fr.uga.miashs.cours4.errors.RestError, ContentType: image/jpeg
			// j'ai essay� de la resoudre en acceptant mettant le header sur accept application/json cot� client mais rien n'y fait
		}
		return photosDao.getPhoto(photoId);
	

	}

	@Path("/{id : [0-9]+}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response delete(@PathParam("id") long albumId) {
		AppAlbum a = albumsDao.read(albumId);
		albumsDao.checkIfOwner(a.getOwner().getEmail()); 
		return albumsDao.delete(albumId);
	}

	@Path("/{id : [0-9]+}/photos/{photoId : [0-9]+}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deletePhoto(@PathParam("id") long albumId, @PathParam("photoId") long photoId) {
		AppAlbum a = albumsDao.read(albumId);
		albumsDao.checkIfOwner(a.getOwner().getEmail()); 
		return photosDao.delete(photoId);
	}

	@GET
	@Path("/{id : [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getAlbum(@PathParam("id") long albumId, @Context UriInfo uriInfo) {
		
		return Response.ok().entity(albumsDao.getAlbum(albumId)).build();
	}
	
	@PATCH
	@PUT
	@Path("/{id: [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@PathParam("id") long id, Map<String, Object> obj, @Context UriInfo uriInfo) {
		AppAlbum a = albumsDao.read(id);
		albumsDao.checkIfOwner(a.getOwner().getEmail());
		return albumsDao.update(id, obj, uriInfo);
	}
	
    @PATCH
    @Path("/{id : [0-9]+}/shared")
    @Produces(MediaType.APPLICATION_JSON)
    public Response UnshareAlbums(@PathParam("id") long  albumId,List<Long> groupsIds,@Context UriInfo uriInfo) {
    	AppAlbum a = albumsDao.read(albumId);
		albumsDao.checkIfOwner(a.getOwner().getEmail());
		
    	for(Long group : groupsIds) {
    		groupsDao.UnShareAlbums(albumId, group, uriInfo);
    	}
    	return Response.status(Response.Status.OK).build();
    	// a faire verifier que l'utilisateur est bien membre de chaque groupe
    }

    
    @POST
    @Path("/{id : [0-9]+}/shared")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response shareAlbums(@PathParam("id") long  albumId,List<Long> groupsIds,@Context UriInfo uriInfo) {
    	AppAlbum a = albumsDao.read(albumId);
		albumsDao.checkIfOwner(a.getOwner().getEmail());
		boolean ok=true;
    	for(Long group : groupsIds) {
    		ok=groupsDao.shareAlbums(List.of(albumId), group, uriInfo,a.getOwner().getId());
    	}
    	if(!ok) {
    	 	return Response.ok().entity("certains traitement n'ont pas �t� fait car vous n'�tes pas membre du groupe sp�cifi�").build();
    	}
    	return Response.status(Response.Status.OK).build();
    	// a faire verifier que l'utilisateur est bien membre de chaque groupe
    }

}
