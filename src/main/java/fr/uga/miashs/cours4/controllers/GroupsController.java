package fr.uga.miashs.cours4.controllers;

import fr.uga.miashs.cours4.dao.AlbumsDao;
import fr.uga.miashs.cours4.dao.GroupsDao;
import fr.uga.miashs.cours4.dao.UsersDao;
import fr.uga.miashs.cours4.model.AppAlbum;
import fr.uga.miashs.cours4.model.AppGroup;
import fr.uga.miashs.cours4.model.AppUser;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.ClaimValue;
import org.eclipse.microprofile.jwt.Claims;

@Path("/groups")
@RolesAllowed("user")
public class GroupsController {

	@Inject
	private GroupsDao groupsDao;

	@Inject
	private UsersDao usersDao;

	@Inject
	private AlbumsDao albumsDao;

	@RolesAllowed("admin")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAll(@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
		return groupsDao.listAll(uriInfo, limit, offset);
	}

	// requete juste pour verifier que le delete se passait bien
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    @Path("/members")
//    public Response getAllMembers(@Context UriInfo uriInfo, @PathParam("limit") int limit, @PathParam("offset") int offset) {
//        return groupsDao.listAllMembers(uriInfo, limit, offset);
//    }

	@PATCH
	@Path("/{id : [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response removeMembers(@PathParam("id") long groupId, List<Long> membersIds, @Context UriInfo uriInfo) {
		AppGroup g = groupsDao.read(groupId);
		groupsDao.checkIfOwner(g.getOwner().getEmail());
		return groupsDao.removeMembers(membersIds, groupId, uriInfo);
	}

	@POST
	@Path("/{id : [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addMember(@PathParam("id") long groupId, List<Long> membersIds, @Context UriInfo uriInfo) {
		AppGroup g = groupsDao.read(groupId);
		groupsDao.checkIfOwner(g.getOwner().getEmail());
		return groupsDao.addMembers(membersIds, groupId, uriInfo);
	}

	@GET
	@Path("/{id : [0-9]+}/albums")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSharedAlbums(@Context UriInfo uriInfo, @PathParam("id") long groupId) {

		Response r = albumsDao.getSharedAlbums(groupId, uriInfo, 10, 0);
		List<AppAlbum> l = (List<AppAlbum>) r.getEntity();
		for (AppAlbum a : l) {
			a.setOwner(null);
		}
		return Response.ok().entity(l).build();
		
	}

//	@DELETE
//	@Path("/{id : [0-9]+}/albums/{albumId : [0-9]+}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response UnshareAlbums(@PathParam("id") long groupId, @PathParam("albumId") long albumId,
//			@Context UriInfo uriInfo) {
//		
//		return groupsDao.UnShareAlbums(albumId, groupId, uriInfo);
//
//	}
//
//	@POST
//	@Path("/{id : [0-9]+}/albums")
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
//	public Response shareAlbums(@PathParam("id") long groupId, List<Long> albumsIds, @Context UriInfo uriInfo) {
//	
//		return groupsDao.shareAlbums(albumsIds, groupId, uriInfo);
//	}

	@GET
	@Path("/{id: [0-9]+}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGroupMembers(@PathParam("id") long groupId, @Context UriInfo uriInfo) {
		AppGroup g = groupsDao.read(groupId);
		groupsDao.checkIfOwner(g.getOwner().getEmail());
		Response r = usersDao.getGroupMembers(groupId, uriInfo, 10, 0);
		List<AppUser> l = (List<AppUser>) r.getEntity();
		for (AppUser u : l) {
			u.setEmail(null);
			u.setUserType(null);
		}
		return Response.ok().entity(l).build();
	}

}
