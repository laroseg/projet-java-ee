package fr.uga.miashs.cours4.security;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.ws.rs.NameBinding;

import fr.uga.miashs.cours4.dao.GenericJpaRestDao;

@NameBinding
@Target({TYPE,METHOD})
@Retention(value =RUNTIME)
public @interface IsOwner {
	
}
