package fr.uga.miashs.cours4.security;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;

import javax.inject.Inject;
import javax.persistence.TypedQuery;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.eclipse.microprofile.jwt.Claim;
import org.eclipse.microprofile.jwt.ClaimValue;
import org.eclipse.microprofile.jwt.Claims;

import fr.uga.miashs.cours4.dao.AlbumsDao;
import fr.uga.miashs.cours4.dao.GenericJpaRestDao;
import fr.uga.miashs.cours4.dao.UsersDao;
import fr.uga.miashs.cours4.errors.NotOwnerException;
import fr.uga.miashs.cours4.model.AppAlbum;

@Provider
@IsOwner
public class OwnerFilter implements ContainerRequestFilter {


	@Inject
	private UsersDao usersDao;
	
	@Inject
	@Claim(standard = Claims.sub)
	private ClaimValue<String> connectedUserEmail;



	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
	
		MultivaluedMap<String, String> pathparam = requestContext.getUriInfo().getPathParameters();
		long id = Long.parseLong(pathparam.get("id").get(0));
		 //System.out.println("contenu du filtre"+ id);
		 String email = usersDao.getEmailFromId(id);
		 if(connectedUserEmail.getValue()==null) {
			 throw new NotOwnerException("vous devez vous authentifier pour acc�der � cette ressource");
		 }
		//System.out.println("email connect "+co);
		if(!connectedUserEmail.getValue().equals(email)) {
			throw new NotOwnerException("vous n'�tes pas le propri�taire de cette ressource");
		}
	
		 


	}
}
