package fr.uga.miashs.cours4.model;

/**
 * Classe utilisée pour gérer des erreurs propres aux classes du modèle.
 * Je ne l'ai pas utilisé pour l'instant.
 */
public class UserModelException extends RuntimeException {
    public UserModelException(String s) {
        super(s);
    }
}
