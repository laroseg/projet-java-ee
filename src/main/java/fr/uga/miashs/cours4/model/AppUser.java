package fr.uga.miashs.cours4.model;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

// les requêtes peuvent être définies ici sous forme de requête nommées
// et chargées dans le DAO via em.createNamedQuery(...)
// ou directement dans le DAO (c.f. GroupsDao). C'est une question de goût..
@NamedQueries({
		@NamedQuery(name = "AppUser.membersOf", query = "SELECT u FROM AppGroup g INNER JOIN g.members u WHERE g.id=:id"),
		@NamedQuery(name = "AppUser.isMemberOf", query = "SELECT g FROM AppGroup g INNER JOIN FETCH g.members u WHERE u.id=:id"),
		@NamedQuery(name = "AppUser.findIdFromEmail", query = "SELECT u.id FROM AppUser u WHERE u.email=:email")
		})
@Entity
public class AppUser {

	@Id
	@GeneratedValue
	private long id;

	@NotBlank(message = "Le prénom doit être renseigné")
	private String firstname;
	@NotBlank(message = "Le nom doit être renseigné")
	private String lastname;

	@PositiveOrZero(message = "{appuser.agenegatif}")
	private int age;

	@NotBlank
	@Email
	@Column(unique = true)
	private String email;

	@NotBlank
	private String passwordHash;

	@Transient
	private String password;

	@Enumerated(EnumType.STRING)
	@Column(columnDefinition = "VARCHAR(5)")
	private AppUserType userType;

	
	@OneToMany(targetEntity = AppAlbum.class, mappedBy = "owner")
	private Set<AppAlbum> albums = new TreeSet<>();
	
	@OneToMany(targetEntity = AppGroup.class,mappedBy = "owner")
	private Set<AppGroup> groups = new TreeSet<>();


	public AppUser() {
		this.userType = AppUserType.USER;
	}

	public AppUser(long id, AppUserType userT) {
		this.id = id;
		this.userType = userT;
	}

	public AppUser(String firstname, String lastname, String email, int age,String password) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.age = age;
		this.userType = AppUserType.USER;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<AppAlbum> getAlbums() {
		return albums;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		AppUser appUser = (AppUser) o;
		return Objects.equals(email, appUser.email);
	}

	@Override
	public int hashCode() {
		return Objects.hash(email);
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public long getId() {
		return id;
	}

	public AppUserType getUserType() {
		return userType;
	}

	public void setUserType(AppUserType userType) {
		this.userType = userType;
	}

	@JsonbTransient
	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<AppGroup> getGroups() {
		return groups;
	}

	public void setGroups(Set<AppGroup> groups) {
		this.groups = groups;
	}

}
