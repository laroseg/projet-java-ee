package fr.uga.miashs.cours4.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@NamedQueries({
    @NamedQuery(name="AppPhoto.findByAlbum",
    query = "SELECT p FROM AppPhoto p WHERE p.album.id=:id"),
})
public class AppPhoto {

	@Id
	@GeneratedValue
	private long id;

	
	private String chemin;

	@ManyToOne
	@NotNull
	private AppAlbum album;

	public AppPhoto(String chemin, @NotNull AppAlbum album) {
		this.chemin = chemin;
		this.album = album;
	}

	public AppPhoto() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getChemin() {
		return chemin;
	}

	public void setChemin(String chemin) {
		this.chemin = chemin;
	}

	public AppAlbum getAlbum() {
		return album;
	}

	public void setAlbum(AppAlbum album) {
		this.album = album;
	}

}
