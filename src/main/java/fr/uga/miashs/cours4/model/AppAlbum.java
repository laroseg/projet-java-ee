package fr.uga.miashs.cours4.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity

@NamedQueries({
    @NamedQuery(name="AppAlbum.findByOwner",
    query = "SELECT a FROM AppAlbum a WHERE a.owner.id=:id"),
    @NamedQuery(name="AppAlbum.sharedWith",
    query="SELECT a FROM AppGroup g INNER JOIN g.sharedAlbums a WHERE g.id=:id")
})
@NamedEntityGraphs({
  
    @NamedEntityGraph(name = "AppAlbum.nameAndId",
            attributeNodes = {@NamedAttributeNode("id"),@NamedAttributeNode("name"),@NamedAttributeNode("description"),@NamedAttributeNode(value="owner",subgraph="onlyEmail")},
    subgraphs = {
            @NamedSubgraph(
                    name = "onlyEmail",
                    attributeNodes = {
                            @NamedAttributeNode("email")
                        
                    }
            )
           
    })

    
})
public class AppAlbum {

	@Id
	@GeneratedValue
	private long id;
	
	@ManyToOne
    @NotNull
    private AppUser owner;
	
	@NotBlank(message = "L'album doit  avoir un nom pour pouvoir �tre cr�e")
    private String name;
	@OneToMany( targetEntity=AppPhoto.class, mappedBy="album",cascade = CascadeType.REMOVE,orphanRemoval=true )
	private List<AppPhoto> photos = new ArrayList<>();
	
	@Column(length = 200)
    @Size(min = 0, max = 200)
	private String description;
	

	@ManyToMany(mappedBy="sharedAlbums")
	private Set<AppGroup> groups = new TreeSet<>();

	public AppAlbum() {}
	public AppAlbum(String name, AppUser owner, String description) {
		this.description = description;
		this.name = name;
		this.owner = owner;

		
	}
	public long getId() {
        return id;
    }
	public AppUser getOwner() {
		return owner;
	}

	public void setOwner(AppUser owner) {
		this.owner = owner;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppAlbum appAlbum = (AppAlbum) o;
        return Objects.equals(name, appAlbum.name) &&
                Objects.equals(id, appAlbum.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }
    
	public List<AppPhoto> getPhotos() {
		return photos;
	}
	public void setPhotos(List<AppPhoto> photos) {
		this.photos = photos;
	}

}