package fr.uga.miashs.cours4.errors;


import javax.ws.rs.core.Response;

public class NotOwnerException extends RuntimeException {

	public NotOwnerException(String message) {
		super(message);
		
	}

	
		
}
