package fr.uga.miashs.cours4.errors;


import fr.uga.miashs.cours4.model.UserModelException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class OwnerExceptionMapper implements ExceptionMapper<NotOwnerException> {
    @Override
    public Response toResponse(NotOwnerException exception) {
        RestError e = new RestError();
        e.type="/authentification-errors";
        e.title = "acces interdit";
        e.detail = exception.getMessage();
        return Response.status(Response.Status.FORBIDDEN).entity(e).build();
    }
}
