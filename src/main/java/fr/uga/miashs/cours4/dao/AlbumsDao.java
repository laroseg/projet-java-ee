package fr.uga.miashs.cours4.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityGraph;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import fr.uga.miashs.cours4.errors.NotOwnerException;
import fr.uga.miashs.cours4.model.AppAlbum;
import fr.uga.miashs.cours4.model.AppGroup;
import fr.uga.miashs.cours4.model.AppPhoto;
import fr.uga.miashs.cours4.model.AppUser;

public class AlbumsDao extends GenericJpaRestDao<AppAlbum> {

	@Inject
	private PhotosDao photosDao;
	

	@Inject
	private GroupsDao groupsDao;
	
	

	public AlbumsDao() {
		super(AppAlbum.class);
	}

	public Response createAlbum(long userId, AppAlbum g, UriInfo uriInfo) {
		AppUser owner = getEm().find(AppUser.class, userId);
		if (owner == null)
			throw new NotFoundException();
		TypedQuery<AppAlbum> q = getEm().createQuery("SELECT a FROM AppAlbum a WHERE a.name=:name and a.owner.id=:userId",AppAlbum.class);
		q.setParameter("name", g.getName());
		q.setParameter("userId", userId);
		try {
			AppAlbum ap = q.getSingleResult();
		}
		catch(NoResultException e) {
			g.setOwner(owner);
			Response r = create(g, uriInfo);
			return r;
		}
		
			throw new BadRequestException(Response.status(Status.BAD_REQUEST).entity("un album du m�me nom existe d�ja").build());
		

	}

	public Response getAlbums(long userId, UriInfo uriInfo, int limit, int offset) {

		EntityGraph graph = getEm().getEntityGraph("AppAlbum.nameAndId");
		TypedQuery<AppAlbum> t = getEm().createNamedQuery("AppAlbum.findByOwner", AppAlbum.class);
		t.setHint("javax.persistence.fetchgraph", graph);
		t.setParameter("id", userId);
		return executeQuery(t, uriInfo, limit, offset);
	}

	public AppAlbum getAlbum(long albumId) {
		
		EntityGraph graph = getEm().getEntityGraph("AppAlbum.nameAndId");
	    AppAlbum ap = read(albumId, graph);
	    if(!ap.getOwner().getEmail().equals(connectedUserEmail.getValue())){
			groupsDao.checkIfMember(connectedUserEmail.getValue(),ap);
		}
	    ap.setOwner(null);
	    
	    return ap;
	    
		

	}

	public Response getSharedAlbums(long groupId, UriInfo uriInfo, int limit, int offset) {
		groupsDao.checkIfMember(connectedUserEmail.getValue(), groupId);
		TypedQuery<AppAlbum> q = getEm().createNamedQuery("AppAlbum.sharedWith", AppAlbum.class);
		q.setParameter("id", groupId);
		return executeQuery(q, uriInfo, limit, offset);
	}
	
	@Override
	public Response delete(long id) {

		TypedQuery<Long> p = getEm().createQuery("SELECT g.id FROM AppPhoto g WHERE g.album.id=:id", Long.class);
		p.setParameter("id", id);
		List<Long> photos = p.getResultList();
		for (Long photo : photos) {
			photosDao.delete(photo);
		}
		return super.delete(id);
	}
	
	

	
	

	

}
