package fr.uga.miashs.cours4.dao;

import fr.uga.miashs.cours4.model.AppAlbum;
import fr.uga.miashs.cours4.model.AppGroup;
import fr.uga.miashs.cours4.model.AppUser;
import fr.uga.miashs.cours4.model.AppUserType;
import fr.uga.miashs.cours4.security.JWTUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.security.enterprise.identitystore.Pbkdf2PasswordHash;
import javax.ws.rs.GET;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 */
@Path("/users")
public class UsersDao extends GenericJpaRestDao<AppUser> {
	@Inject
	private Pbkdf2PasswordHash hashAlgo;

	@Inject
	private JWTUtils jwtUtils;

	@Inject
	private AlbumsDao albumsDao;

	@Inject
	private GroupsDao groupsDao;

	public UsersDao() {
		super(AppUser.class);
	}

	public Response getGroupMembers(long groupId, UriInfo uriInfo, int limit, int offset) {
		TypedQuery<AppUser> q = getEm().createNamedQuery("AppUser.membersOf", AppUser.class);
		q.setParameter("id", groupId);
		return executeQuery(q, uriInfo, limit, offset);
	}

	@Override
	public Response create(AppUser u, UriInfo uriInfo) {

		if (u.getPassword() != null) {
			u.setPasswordHash(hashAlgo.generate(u.getPassword().toCharArray()));
			u.setPassword(null);
		} else {
			u.setPasswordHash(null);
		}
		return super.create(u, uriInfo);
	}

	public Response login(String email, String password) {
		Query q = getEm().createQuery("SELECT u FROM AppUser u WHERE u.email=:email");
		q.setParameter("email", email);
		AppUser u = (AppUser) q.getSingleResult();

		if (!hashAlgo.verify(password.toCharArray(), u.getPasswordHash())) {
			throw new NotAuthorizedException("mot de passe ou login incorrect !");
		}
		List<String> roles = new ArrayList<>();
		roles.add("user");
		if (u.getUserType() == AppUserType.ADMIN) {
			roles.add("admin");
		}
		String jwt = jwtUtils.generateToken(u.getEmail(), roles, 7200);

		 return Response.ok().entity("Authorization: Bearer "+jwt).build();
		//return Response.ok().entity(jwt).build();
	}

	@Override
	public Response delete(long id) {

		TypedQuery<Long> q = getEm().createQuery("SELECT g.id FROM AppGroup g WHERE g.owner.id=:id", Long.class);
		q.setParameter("id", id);
		List<Long> groups = q.getResultList();
		for (Long group : groups) {
			groupsDao.delete(group);
		}
		TypedQuery<Long> a = getEm().createQuery("SELECT g.id FROM AppAlbum g WHERE g.owner.id=:id", Long.class);
		a.setParameter("id", id);
		List<Long> albums = a.getResultList();
		for (Long album : albums) {
			albumsDao.delete(album);
		}
		return super.delete(id);
	}
	
	public String  getEmailFromId(long id) {
		TypedQuery<String> t = getEm().createQuery("SELECT u.email FROM AppUser u WHERE u.id=:id", String.class);
		t.setParameter("id", id);
		String s;
		try {
			
			s = t.getSingleResult();
		}
		catch(NoResultException e) {
			throw new NotFoundException();
		}
		return s;
	}

}
