package fr.uga.miashs.cours4.dao;

import fr.uga.miashs.cours4.errors.NotOwnerException;
import fr.uga.miashs.cours4.model.AppAlbum;
import fr.uga.miashs.cours4.model.AppGroup;
import fr.uga.miashs.cours4.model.AppUser;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.persistence.EntityGraph;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;


public class GroupsDao extends GenericJpaRestDao<AppGroup> {

    // Il y a une dépendance mutuelle entre GroupController et UserController
    // une solution est d'injecter Instance<UserController> à la place de  UserController
    // puis utiliser get() pour obtenir la référence
    @Inject
    private Instance<UsersDao> usersController;

    public GroupsDao() {
        super(AppGroup.class);
    }


    public Response addMembers(List<Long> ids, long groupId, UriInfo uriInfo) {
        // Comme l'égalité et le hashcode sont définis sur l'email
        // cela cause un select du membre dans tous les cas
        //AppUser membre = em.getReference(AppUser.class,userId);
        // la solution la plus efficace revient à faire un native insert
        // et traiter les éventuelles erreurs (duplicate, foreign key)
        Query q = getEm().createNativeQuery("INSERT INTO AppGroup_AppUser(AppGroup_id,Members_id) VALUES(?,?)");
        q.setParameter(1,groupId);
        ids.forEach( userId -> {
            q.setParameter(2,userId);
            q.executeUpdate();
            // La aussi meme avec em.getReference, il y a chargement des membres
            //em.find(AppGroup.class,g.id).getMembres().add(membre);
        });
        return Response.status(Response.Status.OK).build();
    }
    
    public boolean shareAlbums(List<Long> ids, long groupId, UriInfo uriInfo,long userId) {
    	
    	AppGroup a = read(groupId);
    	List<AppGroup> mo = IsMemberOf(userId);
    	boolean ok = false;
    	for(AppGroup group : mo) {
    		if(group.getId()==groupId||a.getOwner().getId()==userId) {
    			ok = true;
    			break;
    		}
    	}
    	if(ok) {
    		   Query q = getEm().createNativeQuery("INSERT INTO AppGroup_AppAlbum(groups_id,sharedAlbums_id) VALUES(?,?)");
    	        q.setParameter(1,groupId);
    	        ids.forEach( albumId -> {
    	            q.setParameter(2,albumId);
    	            q.executeUpdate();
    	        });

    	        
    	}
    	return ok;
    	
     
    }
    
    
    public Response UnShareAlbums(long albumId, long groupId, UriInfo uriInfo) {
     
        Query q = getEm().createNativeQuery("DELETE FROM AppGroup_AppAlbum WHERE groups_id=? and sharedAlbums_id=?");
        q.setParameter(1,groupId);
        q.setParameter(2,albumId);
        q.executeUpdate();
        return Response.status(Response.Status.OK).build();
    }
    
    
    


    public Response createGroup(long userId, AppGroup g, UriInfo uriInfo) {
        AppUser owner = getEm().find(AppUser.class,userId);
        if (owner==null) throw new NotFoundException();
        g.setOwner(owner);
        Response r = create(g,uriInfo);
        return r;
    }

    public Response getGroups(long userId, UriInfo uriInfo, int limit, int offset) {
        EntityGraph graph = getEm().getEntityGraph("AppGroup.groupOnly");
        TypedQuery<AppGroup> q = getEm().createNamedQuery("AppGroup.findByOwner", AppGroup.class);
        // alternative : query directe dans le code
        //TypedQuery<AppGroup> q = getEm().createQuery("SELECT g FROM AppGroup g WHERE g.owner.id=:id", AppGroup.class);
        q.setHint("javax.persistence.fetchgraph", graph);
        q.setParameter("id",userId);
        return executeQuery(q,uriInfo,limit,offset);
    }

    @Override
    public Response listAll(UriInfo uriInfo, int limit, int offset) {
        return super.executeQuery(getEm().createNamedQuery("AppGroup.findAllEagerMembers", AppGroup.class),
                uriInfo,limit,offset);
    }
    
    

	public Response removeMembers(List<Long> membersIds, long groupId, UriInfo uriInfo) {

	
		  Query q = getEm().createNativeQuery("DELETE FROM AppGroup_AppUser WHERE AppGroup_id=? and Members_id = ? ");
	        q.setParameter(1,groupId);
	        membersIds.forEach( userId -> {
	            q.setParameter(2,userId);
	            q.executeUpdate();
	       
	        });
	        return Response.status(Response.Status.OK).build();
	}
	
	public List<AppGroup> IsMemberOf(long userId) {
		TypedQuery<AppGroup> q = getEm().createNamedQuery("AppUser.isMemberOf",AppGroup.class);
		q.setParameter("id",userId);
		//q.setHint("javax.persistence.fetchgraph", getEm().getEntityGraph("AppGroup.groupWithOwner")); // je ne voulais pas selectionner les membres mais ca ne marche pas
																										// je pense car j'ai fait un inner join dans la requete
		List<AppGroup> l = q.getResultList();
		for(AppGroup group : l) {
			setNullForAllLazyLoadEntities(group);
			group.setMembers(null);
			group.setOwner(null);
		}
	
		return l;
	}
	public void checkIfMember(String email, AppAlbum a) {
		TypedQuery<Long> e = getEm().createNamedQuery("AppUser.findIdFromEmail",Long.class);
		e.setParameter("email",email);
		long userId = e.getSingleResult();
		List<AppGroup> groupOfUser = IsMemberOf(userId);
		TypedQuery<AppAlbum> q = getEm().createNamedQuery("AppAlbum.sharedWith", AppAlbum.class);
		for(AppGroup group : groupOfUser) {
			
			q.setParameter("id", group.getId());
			List<AppAlbum> ap = q.getResultList();
			if(ap.contains(a)) {
				return;
			}
		}
	       EntityGraph graph = getEm().getEntityGraph("AppGroup.groupOnly");
	        TypedQuery<AppGroup> t = getEm().createNamedQuery("AppGroup.findByOwner", AppGroup.class);
	        t.setHint("javax.persistence.fetchgraph", graph);
	        t.setParameter("id",userId);
	    	List<AppGroup> groups = t.getResultList();
	    	for(AppGroup group : groups) {
				
				q.setParameter("id", group.getId());
				List<AppAlbum> ap = q.getResultList();
				if(ap.contains(a)) {
					return;
				}
			}
	    	
	    	
	
		throw new NotOwnerException("vous n'�tes pas autoris� � acc�der � cette ressource");
	}
	
	public void checkIfMember(String email, long groupId) {
		TypedQuery<Long> e = getEm().createNamedQuery("AppUser.findIdFromEmail",Long.class);
		e.setParameter("email",email);
		long userId = e.getSingleResult();
		TypedQuery<AppGroup> p = getEm().createNamedQuery("AppGroup.findMembers",AppGroup.class);
		p.setParameter("id", groupId);
		AppGroup group = p.getSingleResult();
		setNullForAllLazyLoadEntities(group);
		AppUser u = getEm().find(AppUser.class, userId);
		if(group.getMembers().contains(u)||group.getOwner().getId()==userId) {
			return;
		}
		
		
		
	
		throw new NotOwnerException("vous n'�tes pas autoris� � acc�der � cette ressource");
	}



	// requete juste pour verifier que le delete se passait bien 
//	public Response listAllMembers(UriInfo uriInfo, int limit, int offset) {
//        Query q = getEm().createNativeQuery("SELECT * FROM AppGroup_AppUser");
//		return Response.ok().entity(q.getResultList()).build();
//	}
    
	
}
