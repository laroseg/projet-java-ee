package fr.uga.miashs.cours4.dao;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.persistence.EntityGraph;
import javax.persistence.TypedQuery;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.uga.miashs.cours4.model.AppAlbum;
import fr.uga.miashs.cours4.model.AppPhoto;

public class PhotosDao extends GenericJpaRestDao<AppPhoto> {
	@Inject
	private GroupsDao groupsDao;
	public PhotosDao() {
		super(AppPhoto.class);
	}
    public static Map <String, String> types = Map.of("image/png", "png", "image/jpeg", "jpeg");
    public static String dirPhotos = "C:\\Users\\pcguillaume\\Desktop\\javaEETEST\\";


	public Response addPhoto(long albumId,UriInfo uri,InputStream inputStream,String fileType) {
		AppAlbum a = getEm().find(AppAlbum.class, albumId);
		if (a == null)
			throw new NotFoundException();
		checkIfOwner(a.getOwner().getEmail());
		AppPhoto ap = null;
		try {
			BufferedImage image = ImageIO.read(inputStream);
			  String dir = dirPhotos+albumId+"_"+a.getName()+"\\";
		        Path path = Paths.get(dir);

		        if (!Files.exists(path)) {    
		        	Files.createDirectory(path);
	       
		        } 
			ap = new AppPhoto();
			ap.setAlbum(a);
			ap.setChemin("");
			Response r = create(ap,uri);
			ImageIO.write(image,types.get(fileType), new File(dir+r.readEntity(AppPhoto.class).getId()+"."+types.get(fileType)));
			 ap = getEm().find(AppPhoto.class, r.readEntity(AppPhoto.class).getId());
			 ap.setChemin(dir+r.readEntity(AppPhoto.class).getId()+"."+types.get(fileType));
			 getEm().flush();
			return Response.ok().entity(ap.getChemin()).build();
		
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			

		}
	
		return Response.status(Response.Status.BAD_REQUEST).build();
	}
	
	public InputStream getPhoto(long photoId) throws IOException {
		AppPhoto ap = getEm().find(AppPhoto.class, photoId);
		if (ap == null)
			throw new NotFoundException();
		
		java.nio.file.Path p = Paths.get(ap.getChemin());
		return Files.newInputStream(p);
	}
	@Override
	public Response delete(long photoId) {
		
		AppPhoto ap  = read(photoId);
		 try
	        { 
			 	System.out.println(ap.getChemin());
	            Files.deleteIfExists(Paths.get(ap.getChemin())); 
	        } 
	        catch(NoSuchFileException e) 
	        { 
	            System.out.println("Cette photo n'existe pas "); 
	        } 
	        catch(IOException e) 
	        { 
	            System.out.println("Invalid permissions."); 
	        } 
		return super.delete(photoId) ;
	}
	
	 public Response getAlbumPhotos(long albumId, UriInfo uriInfo, int limit, int offset) {
	        TypedQuery<AppPhoto> q = getEm().createNamedQuery("AppPhoto.findByAlbum", AppPhoto.class);
	        q.setParameter("id",albumId);
	        return executeQuery(q,uriInfo,limit,offset);
	    }

	 public Response listAll(long albumId, UriInfo uriInfo, int limit, int offset) {
			AppAlbum a = getEm().find(AppAlbum.class, albumId);
			if(a==null) {
				throw new NotFoundException("l'entit้ n'existe pas dans la BDD");
			}
			if(!a.getOwner().getEmail().equals(connectedUserEmail.getValue())){
				groupsDao.checkIfMember(connectedUserEmail.getValue(),a);
			}
			//System.out.println(a.getOwner().getEmail());
		

			TypedQuery<AppPhoto> t = getEm().createQuery("SELECT a FROM AppPhoto a WHERE a.album.id=:id",AppPhoto.class);
			t.setParameter("id", albumId);
			return executeQuery(t, uriInfo, limit, offset);
		}
	   
}
